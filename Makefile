.PHONY: build
build:
	@pip install -r requirements.txt

.PHONY: test
test:
	@python manage.py test

.PHONY: flake
flake:
	@flake8 --statistics --show-source api_gasolina

.PHONY: docker-build
image:
	@docker build -t widancassio/consumo-gasolina-api .

.PHONY: docker-push
push:
	@docker push widancassio/consumo-gasolina-api