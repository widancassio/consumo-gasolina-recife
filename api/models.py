from django.db import models


class Supply(models.Model):
    id = models.BigAutoField(db_column='id_abastecimento', null=False, primary_key=True)
    km_supply = models.IntegerField(db_column='nb_km_abastecimento', null=False)
    quantity_liters = models.FloatField(db_column='nb_quantidade_litros', null=False)
    value = models.FloatField(db_column='nb_valor', null=False)
    date_supply = models.DateTimeField(db_column='dt_abastecimento', null=True)
    created_at = models.DateTimeField(db_column='dt_cadastro', null=True, auto_now_add=True)

    class Meta:
        db_table = 'abastecimento'
        managed = True
        verbose_name = 'Abastecimento'
        verbose_name_plural = 'Abastecimentos'
